import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Contact from '../views/Contact.vue'
import Login from '../views/Login.vue'
import AddContact from '../views/AddContact.vue'
import UpdateContact from '../views/UpdateContact.vue'

//semantic ui
import SuiVue from 'semantic-ui-vue'
//import 'semantic-ui-css/semantic.min.css'
Vue.use(SuiVue)
//
Vue.use(VueRouter)


const routes = [
  {
    path: '/',
    name: 'Login',
    component: Login
  },
  {
    path: '/contact',
    name: 'Contact',
    component: Contact
  },
  {
    path: '/addcontact',
    name: 'AddContact',
    component: AddContact
  },
  {
    path: '/updatecontact/:id',
    name: 'UpdateContact',
    component: UpdateContact
  }
]

const router = new VueRouter({
  routes
})

export default router
